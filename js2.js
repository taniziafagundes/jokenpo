let jogada;
var pontos = 0;
var pontos_c = 0;
var jogadaC;
const options_jogo = ["pedra", "papel", "tesoura"];
const element_click = document.querySelector(".click");  //duvida:  diferença entre  queryselector e getelementby...
const user = document.querySelector(".sua_opcao");
const Computador = document.querySelector(".opcao_computador");
const ganhou = document.querySelector(".ganhou");

const user_pontuacao  = document.querySelector(".pontuacao_sua");
const comp_pontuacao = document.querySelector(".pontuacao_computador");

element_click.addEventListener("click", event => {
    jogada = event.target.id;
    jogadaC = options_jogo[ Math.floor(Math.random()* options_jogo.length)];

    //LIMPAR resultados anteriores
    ganhou.innerHTML= null;
    user_pontuacao.innerHTML = null;
    comp_pontuacao.innerHTML = null;


    //Enviar front opcoes inseridas
    user.innerHTML = `<img  src="img/${jogada}.png" alt=""></img>`;  //não conseguir utilizar createElement para img
    Computador.innerHTML = `<img src="img/${jogadaC}.png" alt=""></img>`;

    resultado = regras(jogada,jogadaC);
     //console.log(resultado)

    if(resultado[0] == "VOCE GANHOU"){
        var teste = document.createElement('h4');
        var conteudo = document.createTextNode("Parabéns, VOCE GANHOU");
        teste.style.backgroundColor =  "rgba(72, 163, 72, 0.521)"
        teste.appendChild(conteudo);
        ganhou.appendChild(teste)

    }else if(resultado[0] == "VOCE PERDEU"){
        var teste = document.createElement('h4');
        var conteudo = document.createTextNode("NÃO FOI DESSA VEZ, VOCE PERDEU");
        teste.style.backgroundColor = "rgba(180, 65, 65, 0.549)"
        teste.appendChild(conteudo);
        ganhou.appendChild(teste)

    }else if(resultado[0] == "EMPATE"){
        var teste = document.createElement('h4');
        var conteudo = document.createTextNode("EMPATE");
        teste.appendChild(conteudo);
        ganhou.appendChild(teste)
    }

    var teste = document.createElement("p");
    var conteudo = document.createTextNode(resultado[1]);
    teste.appendChild(conteudo);
    user_pontuacao.appendChild(teste);

    var teste = document.createElement("p");
    var conteudo = document.createTextNode(resultado[2]);
    teste.appendChild(conteudo);
    comp_pontuacao.appendChild(teste)

})
    

function regras(jogada,jogadaC){
   
    var ganhador = ""
    var res = []
    if(jogada == null ){
        console.log("Jogada Invalida");
    }else{
        if(jogada == options_jogo[0] && jogadaC == options_jogo[1]){
            pontos_c = pontos_c + 1;
            ganhador = "VOCE PERDEU";
            
        }else if(jogada ==options_jogo[1] && jogadaC == options_jogo[0]){
            pontos = pontos + 1;
            ganhador = "VOCE GANHOU";
            
        }else if(jogada == options_jogo[2] && jogadaC== options_jogo[1]){
            pontos = pontos + 1;
            ganhador = "VOCE GANHOU";
            
        }else if(jogada == options_jogo[0] && jogadaC == options_jogo[2]){
            pontos = pontos + 1;
            ganhador = "VOCE GANHOU";
            
        }else if(jogada == options_jogo[0] && jogadaC== options_jogo[1]){
            pontos_c = pontos_c + 1;
            ganhador = "VOCE PERDEU";
            
        }else if(jogada == options_jogo[1] && jogadaC== options_jogo[2]){
            pontos_c = pontos_c + 1;
            ganhador = "VOCE PERDEU";
            
        }else if(jogada == options_jogo[2] && jogadaC== options_jogo[0]){
            pontos_c = pontos_c + 1;
            ganhador = "VOCE PERDEU";
            
        }else if(jogada == jogadaC){
            ganhador = "EMPATE";            
        }
    }
    res[0] = ganhador;
    res[1] = pontos;
    res[2] = pontos_c;
    return res;
}



